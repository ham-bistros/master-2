#!/usr/bin/env python3

import re
import requests
from bs4 import BeautifulSoup
import sys
from urllib.parse import unquote
import shutil
import os

def dl_img(url, dirname, index):
    i = '%02d' % index
    # full_url = 'https://www.wikihow.com' + url
    full_url = url
    response = requests.get(full_url, stream=True)
    name = re.sub(r'https://.*-728px(.*)', r'\1', full_url)
    print('%s is downloading…' % name)

    with open('wikihows/%s/images/%s-%s' % (dirname, i, name), 'wb') as out_file:
        shutil.copyfileobj(response.raw, out_file)
    del response

def get_all_images(url, dirname):
    data = requests.get(url)
    soup = BeautifulSoup(data.text, 'lxml')

    img_tags = soup.select("a.image div img.whcdn")
    if not img_tags:
        img_tags = soup.select("a.image img.whcdn")


    for i, tag in enumerate(img_tags):
        link = tag.get('data-srclarge')
        dl_img(link, dirname, i)

def collectow(url):
    data = requests.get(url)
    soup = BeautifulSoup(data.text, 'lxml')

    titre = soup.select('h1[class^="title_"] a')[0].decode_contents()
    print('\n'+titre.upper())

    intro = soup.select("div#intro p")[2].decode_contents()
    methodes = soup.select("div.altblock div")

    filename = unquote(url).replace("https://fr.wikihow.com/", "")
    total_texte = ''

    total_texte += '# '+titre+'\n'
    total_texte += intro

    for i, met in enumerate(methodes):
        m = re.sub("<\/*span>", '', met.decode_contents()).rstrip()
        met_title = soup.select("div.section.steps h3 div.altblock~span:nth-of-type(1)")[i].get('id').replace('-', ' ')
        # met_title = soup.select("div.section.steps h3 div.altblock~span")[i].decode_contents()

        total_texte += '\n## '+m+' : '+met_title+'\n'
        # print('\n## '+m+' : '+met_title+'\n')
        steps = soup.select("div#étapes_"+str(i+1)+" li div b.whb")

        for num, step in enumerate(steps):
            alors = step.decode_contents()+'\n'
            alors = re.sub(r'<span.*>(.*)</span>', r'\1', alors)
            alors = re.sub(r'<div.*>(.*)</div>', r'\1', alors)
            alors = re.sub(r'<a.*>(.*)</a>', r'\1', alors)
            alors = re.sub(r'<b.*>(.*)</b>', r'\1', alors)
            alors = re.sub('<img.*>', '', alors)

            if alors == '\n':
                continue
            else:
                total_texte += str(num+1)+'. '+alors
                # print(str(num+1)+'. '+alors)

    total_texte = re.sub('<\/*span>', '', total_texte)
    total_texte = re.sub('<\/*i>', '', total_texte)

    if not os.path.isdir('wikihows/%s' % filename):
        os.mkdir('wikihows/%s' % filename)
    if not os.path.isdir('wikihows/%s/images' % filename):
        os.mkdir('wikihows/%s/images' % filename)

    with open('wikihows/%s/texte.md' % filename, 'w', encoding='utf-8') as f:
        f.write(total_texte)

    get_all_images(url, filename)

################# CÉPARTI ##################

urlzzz = [
    "https://fr.wikihow.com/d%C3%A9sinstaller-Google-Chrome",
    "https://fr.wikihow.com/se-d%C3%A9connecter-de-Google-Play",
    "https://fr.wikihow.com/effacer-ses-traces-sur-Google",
    "https://fr.wikihow.com/supprimer-un-compte-Google-ou-Gmail",
    "https://fr.wikihow.com/aller-sur-Google-en-Chine",
    "https://fr.wikihow.com/devenir-un-hacker",
    "https://fr.wikihow.com/charger-une-batterie-sans-chargeur",
    "https://fr.wikihow.com/fermer-Google-Chrome",
    "https://fr.wikihow.com/fabriquer-un-laser",
    "https://fr.wikihow.com/inventer-une-langue",
]

# for url in urlzzz:
#     collectow(url)

collectow(urlzzz[8])
