#!/usr/bin/env python3

import bottle
import subprocess

@bottle.get("/apercu/<filepath:path>")
def apercu(filepath):
    return bottle.static_file(filepath, root="./apercu")

@bottle.get("/projet/<filepath:path>")
def projet(filepath):
    return bottle.static_file(filepath, root="./projet")

@bottle.get("/apercu/<filepath:re:.*\.woff2>")
def woff2(filepath):
    return bottle.static_file(filepath, root="apercu")

# @bottle.get("/projet/centerlined/<filepath:re:.*\.svg>")
# def centerline(filepath):
#     return bottle.static_file(filepath, root="projet/centerlined")

# @bottle.get("/projet/outlined/<filepath:re:.*\.pbm>")
# def outline(filepath):
#     return bottle.static_file(filepath, root="projet/outlined")

@bottle.route('/')
def index():
    subprocess.run(["projet/generate.sh"], shell=True)
    return bottle.template('apercu/index.html')

bottle.run(bottle.app(), host='localhost', port=5000, debug=True, reload=True)

