# Comment fabriquer un laser
Le mot « laser » est en fait l'acronyme de light amplification by stimulated emission of radiation, en français « amplification de la lumière par émission stimulée de rayonnement ». Le premier laser a été développé en 1960 dans les laboratoires Hughes Research en Californie en utilisant un cylindre de rubis recouvert d'argent qui faisait office de résonateur <sup aria-label="Lien vers la référence 1" class="reference" id="_ref-1"><a href="#_note-1">[1]</a><span class="ts_popup" hidden="" id="ts_popup_1" style="display: none;">
<span class="ts_close">X
<span class="ts_name">Source de recherche
<span class="ts_description">
<a class="ts_source" href="#" rel="nofollow noreferrer noopener" target="_blank"></a>

</sup>. À l'heure actuelle, les lasers sont utilisés à de nombreuses fins, par exemple pour faire des mesures ou lire des données encodées, et il existe plusieurs façons de fabriquer son propre laser, selon vos compétences et votre budget.

## Partie 1 : Comment fonctionne un laser
1. Procurez-vous une source d'énergie.
2. Faites passer l'énergie à travers un milieu amplificateur.
3. Mettez en place des miroirs pour contenir la lumière.
4. Utilisez une lentille convergente pour diriger la lumière au travers du milieu amplificateur.

## Partie 2 : Fabriquer un laser
1. Renseignez-vous pour pouvoir trouver un vendeur.
2. Montez la carte de contrôle.
3. Connectez la carte de contrôle à la diode.
4. Connectez la source électrique (la batterie) à la carte de contrôle.
5. Réglez la lentille pour créer le faisceau lumineux du laser.
6. Procurez-vous un vieil enregistreur DVD ou Blu-Ray.
7. Démontez la diode.
8. Procurez-vous une lentille convergente.
9. Procurez-vous une carte de contrôle ou montez-en-une.
10. Connectez la diode à la carte de contrôle.
11. Connectez la source électrique à la carte de contrôle.
12. Réglez la lentille pour ajuster le faisceau du laser.
