#### Comment détourner les outils techno-capitalistes dans un but de partage ?

##### V1
1. Aller voir ses amis
2. Comparer ses abonnements à des services numériques
3. À plusieurs, ça fait un bon paquet de services à mettre en commun
4. Partager ses cookies[^1](les mots de passe, c'est important)
5. Tout le monde peut se connecter aux comptes de tout le monde
6. L'algorithme de recommandation à l'impression d'avoir 5 cerveaux

##### V2
1. Aller voir ses amis
2. Comparer ses abonnements à des services numériques
3. À plusieurs, ça fait un bon paquet de services à mettre en commun
4. Fusionner les cookies[^1]
5. Les ingérer collectivement
6. Bienvenue dans une nouvelle ère

##### V3
1. Organiser des réunions tupperware
2. Nouvelle formule : c'est maintenant des réunions cookies[^1] !
3. S'échanger les cookies afin d'avoir le plus d'abonnements/connexions possible en commun, sans pour autant partager les précieux mots de passe
4. Inviter les participant·es à diffuser leurs cookies[^1] autour d'elleux

##### V4
1. Rencontrer plusieurs personnes
2. Leur demander sur quels services/plateformes iels ont un compte
3. Retrouver le fichier de cookies[^1] dans leur ordinateur
4. Les copier
5. Les combiner dans un fichier à l'aide d'un logiciel approprié
6. Diffuser ce fichier le plus possible
7. Vous faites maintenant partie d'une communauté

[^1]: Les COOKIES sont des fichiers stockés par un site web sur un ordinateur. Ces fichiers permettent de conserver des informations sur l'ordinateur, qui seront retrouvées par le site web lors de la prochaine connexion. Ça permet entre autre de rester authentifié·e sur un site même après avoir rallumé le navigateur, sans avoit besoin de rentrer à nouveau un mot de passe.

 PROCÉDER À UNE COLLECTIVISATION DE PROCESSUS DE CAPTURE AFIN DE PARASITER L'ALIÉNATION DE NOS INDIVIDUALITÉS
