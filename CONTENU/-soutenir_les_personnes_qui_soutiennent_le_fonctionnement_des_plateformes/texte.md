- **MICRO-TRAVAIL**
- **PLATEFORME**
- **MACHINE LEARNING**
- **RÉSEAU SOCIAL**

Lorsque l'on signale du contenu sur un réseau social, bien souvent ce sont des robots qui s'occupent de vérifier si c'est effectivement à censurer. Mais des fois les robots ne sont pas capables de trier correctement. Dans ce cas ce sont des micro-travailleureuses qui doivent regarder tous ces contenus choquants et dire si ça doit être retiré ou non du site. Les pauvres voient du malheur toute la journée, alors redonne-leur un peu de soleil!

### En leur envoyant de la joie

1. Allume ton réseau social préféré.
2. Dirige-toi vers du contenu positif, comme des chatons mignons.
  + Internet peut aussi être plein de belles choses!
3. Signale tout ce que tu vois sur la page comme inapproprié.
  + Rajoute une description fournie, il faut que ça ait l'air d'une vraie plainte.
4. Change de page de temps en temps, les chiots aussi sont mignons.
5. Tu peux aussi signaler des articles de développement personnel ou de motivation.
6. La·e modérateurice de contenu va trouver un peu de réconfort dans sa journée.
7. Savoure le sentiment d'avoir fait une bonne action.
