#!/usr/env/python

import fontforge
# import os
import sys

font = fontforge.open(sys.argv[1])
basename = sys.argv[1].split('.')[0].split('/')[-1]

print(basename)

# Exporte tous les glyphs non-vides en svg dans un dossier créé pour l'occasion
for glyph in font.glyphs():
    # if glyph.left_side_bearing != 0.0 and glyph.right_side_bearing != 0.0:
    if glyph.isWorthOutputting():
        glyph.export('%s-traced/tmp/%s.svg' % (basename, glyph.glyphname))
