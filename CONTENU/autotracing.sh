
#!/bin/bash

dir=$1

mkdir -p ${dir}/../images-vecto

for file in `find $dir -type f -name "*.pgm"`;
do
  fname=${file##*/}
  # echo ${fname%.*}
  autotrace -centerline $file -output-file ${dir}/../images-vecto/${fname%.*}.svg;
  sed -i 's/<svg width="\(.*\)" height="\(.*\)"/<svg width="\1" height="\2" viewBox="0 0 \1 \2"/' ${dir}/../images-vecto/${fname%.*}.svg;
done
