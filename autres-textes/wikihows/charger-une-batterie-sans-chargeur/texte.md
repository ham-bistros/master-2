# Comment charger une batterie sans chargeur
La batterie de votre appareil photo ou de votre portable est à plat tout juste au moment où vous en avez le plus besoin ? Vous avez besoin d'une batterie de toute urgence ?  Si vous vous retrouvez dans une situation où vous n'avez pas de chargeur sur vous et que vous devez trouver une solution, il existe quelques astuces qui peuvent vous sauver.

## Méthode 1 : Utiliser une pile pour recharger une batterie
1. Retirez la batterie de l'appareil.
2. Trouvez des piles AA, AAA ou 9 volts.
3. Identifiez les pôles positifs et négatifs de chaque pile.
4. Ajustez le voltage.
5. Trouvez deux fils métalliques.
6. Attachez les fils à la pile.
7. Vérifiez la charge.

## Méthode 2 : Utiliser l'astuce du frottement
1. Retirez la batterie de l'appareil électronique.
2. Frottez la batterie vigoureusement.
3. Remettez la batterie dans l'appareil électronique.
