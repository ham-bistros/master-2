var testing, pad, inReload, inText, inScale, inPad, iframe, group

function scale(value){
	testing.style.transform = 'scale(' + value + ')'
}

function rand(){
	return Math.floor((Math.random() * 10000) + 1)
}

function loadLetters(letters){
	var tabLetters = letters.split('')
	testing.innerHTML = ''
	tabLetters.forEach(function(lettre, i){
   document.getElementById("back").innerHTML += '<span>' + lettre + '</span>'
		var itemCode = lettre.charCodeAt(0)
		testing.innerHTML += '<img src="../projet/svg/' + itemCode + '.svg?rand=' + rand + '" />'
	})
}

function quniLoad(number){
  testing.innerHTML += '<img src="../projet/svg/' + number + '.svg?rand=' + rand() + '" />'
}

document.addEventListener("DOMContentLoaded", (event) => {
	group = location.hash.replace('#', '')
	testing = document.querySelector('#testing')
	inReload = document.querySelector('#inReload')
	inText = document.querySelector('#inText')
	inScale = document.querySelector('#inScale')

	inReload.addEventListener('click', (event) => {
    let sup = document.querySelectorAll("#back span")
    if (sup) {
      [...sup].forEach(function(el) {
        el.remove()
      })
    }

		loadLetters(inText.value)
    quniLoad(document.getElementById("quni").value)
	})

	inScale.addEventListener("change", (event) => {
		scale(inScale.value)
	})

  backos = document.getElementsByTagName("span");
  boutong = document.getElementById("cashback");

  boutong.addEventListener("click", (event) => {
    [...backos].forEach(function(lel) {
      lel.classList.toggle("hide");
    });
  });

})
