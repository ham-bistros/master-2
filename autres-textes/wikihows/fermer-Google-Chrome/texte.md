# Comment fermer Google Chrome
Google Chrome supporte la navigation par onglets, ce qui signifie que vous pouvez avoir plusieurs pages ouvertes en même temps dans une seule fenêtre. Vous pouvez fermer les onglets ou la fenêtre, quitter le programme et, si nécessaire, forcer le processus de fermeture (à utiliser en dernier recours).    

## Méthode 1 : Fermer les onglets sur Android et iOS
1. Appuyez sur le bouton qui affiche les onglets.
2. Appuyez sur l’icône en forme de x.
3. Fermez simultanément tous les onglets.
4. Fermez les onglets de navigation privée (sur Android uniquement).

## Méthode 2 : Fermer l’application Chrome sur Android
1. Appuyez sur la touche des applications récentes.
2. Faites glisser l’écran vers le haut ou vers le bas.
3. Faites glisser la fenêtre Chrome vers la droite.

## Méthode 3 : Forcer la fermeture de Chrome sur Android
1. Ouvrez l’application Paramètres.
2. Appuyez sur Applications.
3. Appuyez sur Chrome dans la liste des applications.
4. Appuyez sur Forcer l’arrêt.

## Méthode 4 : Fermer Chrome sur iOS
1. Doublecliquez sur le bouton d’accueil.
2. Faites glisser l’écran vers la gauche ou vers la droite.
3. Faites glisser la fenêtre Chrome vers le haut.

## Méthode 5 : Forcer la fermeture de Chrome sur iOS
1. Doublecliquez sur le bouton d’accueil.
2. Appuyez et maintenez enfoncé le bouton d’alimentation.
3. Appuyez une nouvelle fois longuement sur le bouton d’alimentation.

## Méthode 6 : Fermer les onglets, les fenêtres et les processus de Chrome sur un ordinateur
1. Cliquez sur l’icône en forme de « x » sur un onglet.
2. Cliquez sur le x dans le coin de la fenêtre.
3. Cliquez sur le bouton ≡ et sélectionnez Quitter.

## Méthode 7 : Forcer la fermeture de Google Chrome sur un ordinateur
1. Ouvrez le gestionnaire des tâches ou le menu « Forcer à quitter ».
2. Sélectionnez Google Chrome.
3. Arrêtez le processus.
