<!DOCTYPE html>
<html>

  <head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width" />
    <title>PHP-cases-bd</title>
    <link rel="stylesheet" href="https://meyerweb.com/eric/tools/css/reset/reset.css" type="text/css" media="all" charset="utf-8">
    <link rel="stylesheet" href="style.css" type="text/css" media="screen" charset="utf-8">
    <link rel="stylesheet" href="print.css" type="text/css" media="print" charset="utf-8">

    <?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
?>

  </head>

  <body>
    <main>
      <article>
      <?php
        /* $titre = "Comment détourner les outils techno-capitalistes dans un but de partage ?"; */
        /* $etape1 = array("1. Aller voir ses amis", "2. Comparer ses abonnements à des services numériques", "3. À plusieurs, ça fait un bon paquet de services à mettre en commun", "4. Partager ses cookies (les mots de passe, c'est important)", "5. Tout le monde peut se connecter aux comptes de tout le monde"); */

$etape1 = array("Collecte des smartphones. Demande à ton entourage par exemple ! + N'oublie pas de leur demander le mot de passe s'il y en a un. Ce serait dommage d'être déjà bloqué·e dans ta démarche.", "Installe Google Maps sur tous les appareils. + Lance le même itinéraire sur chacun d'entre eux. + Vérifie que tous indiquent que le trafic est important à l'endroit où tu te trouves.", "Investis dans des multiprises. Il va falloir garder ces téléphones plein de batterie !", "Trouve un vélo avec un porte-bagage à l'arrière.", "Mets les appareils dedans, branchés aux multiprises.", "Installe un système de dynamo sur le vélo.", "Branche les prises à la dynamo.", "Maintenant tu peux te promener tranquillement.", "N'oublie pas le casque ! La sécurité avant tout. Google croit qu'il y a un embouteillage ! Les automobilistes vont être dévié·es ailleurs");

$imgdir = 'detournement-gmaps/images';

$fichiers = array_diff(scandir($imgdir), array('..', '.'));
/* print_r($fichiers); */

        foreach ($fichiers as $i => $file) {
          $num_file = preg_replace('/^(\d+[a-z]*).*/', '$1', $file);
          $sectionis =  substr($num_file, -1);
          /* echo $num_file; */

          if (intval($sectionis)) { /* on vérifie si l'image est en plusieurs parties */
            /* Pour les images "pleines cases" */
            echo '<section class="full" id="item-'.$num_file.'">';
            echo '<p class="etape">'.$etape1[$num_file - 1].'</p>';
            /* echo file_get_contents($imgdir.'/'.$file); */

            echo '<img src="'.$imgdir.'/'.$file.'">';
            echo '</section>';

          } else if ($sectionis == 'a') {
            /* Pour les images "sectionisées" (plusieurs images dans une case) */
            $base_nbr = substr($num_file, 0, -1);
            $steps = explode('+', $etape1[$base_nbr - 1]);
            $step_count = 0;

            echo '<div class="full wrapper">';
            echo '<p class="etape">'.$steps[0].'</p>';

            foreach ($fichiers as $f) {
              if (preg_match('/^('.$base_nbr.'+[a-z]+).*/', $f)) {
                echo '<section class="sub" id="item-'.$num_file.'">';

                if ($step_count > 0) {
                  /* echo '<span class="COUNT">'.$step_count.'</span>'; */
                  echo '<span class="precisions">'.$steps[$step_count].'</span>';
                }
                $step_count += 1;

                echo '<img src="'.$imgdir.'/'.$f.'">';

                /* echo file_get_contents($imgdir.'/'.$f); */
                echo '</section>';
              }
            }

            echo '</div>';
          }
        }

    ?>
    </article>

    </main>
  <!-- <script src="paged.polyfill.js"></script> -->

  </body>

</html>
