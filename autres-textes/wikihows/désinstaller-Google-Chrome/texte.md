# Comment désinstaller Google Chrome
Pour libérer de la mémoire ou parce que vous voulez utiliser un autre navigateur, vous pouvez désinstaller Google Chrome de votre ordinateur ou de votre téléphone. Si vous utilisez un appareil sous Android, vous ne pourrez pas le désinstaller, car il s’agit du navigateur par défaut. En revanche, il reste possible de le désactiver pour qu’il n’apparaisse plus dans le tiroir d’applications.

## Méthode 1 : Sur Windows
1. Fermez toutes les fenêtres ouvertes de Google Chrome.
2. Ouvrez le menu de démarrage .
4. Allez dans Paramètres .
6. Sélectionnez .
7. Applications
8. Faites défiler la liste et cliquez sur .
9. Google Chrome
10. Cliquez 2 fois sur .
11. Désinstaller
12. Choisissez  quand vous y serez invité.
13. Oui
14. Sélectionnez  quand vous y serez invité.
15. Désinstaller
16. Forcez la fermeture de Chrome si nécessaire.

## Méthode 2 : Sur un Mac
1. Fermez Google Chrome.
2. Ouvrez le Finder .
4. Sélectionnez .
5. Aller
6. Cliquez sur .
7. Applications
8. Cherchez Google Chrome.
9. Déplacez Google Chrome vers la Corbeille.
10. Forcez la fermeture de Chrome si nécessaire.

## Méthode 3 : Sur un iPhone
1. Cherchez l’application Google Chrome .
3. Posez longuement votre doigt sur Google Chrome.
4. Appuyez sur .
5. X
6. Sélectionnez  quand vous y serez invité.
7. Supprimer

## Méthode 4 : Sur Android
1. Allez dans les paramètres de votre Android.
2. Sélectionnez .
3. Applications
4. Cherchez l’application Chrome .
6. Sélectionnez .
7. DÉSINSTALLER
8. Appuyez sur .
9. DÉSINSTALLER
