- **ALGORITHME DE RECOMMANDATION**

Les GPS modernes permettent facilement de contourner les embouteillages ou les travaux en temps réel. Pour ça ils peuvent indiquer les itinéraires les moins fréquentés, et donc conseiller des routes qui étaient d'habitude désertes. Si tu habites près d'une de ces routes, tu es sûrement dérangé·e par les voitures, surtout le weekend! Voyons comment tu peux retrouver le calme en empêchant les GPS de faire tout le temps passer les voitures par chez toi.

### Fais-croire au GPS qu'il y a un embouteillage
1. Emprunte des smartphones à tes ami·es et voisin·es. Essaie d'en avoir le plus possible.
  + Préviens-les que ce ne sera pas long! Seulement le samedi après-midi par exemple.
  + N'oublie pas de leur demander le mot de passe s'il y en a un.
2. Investis dans une grosse multiprise.
3. Trouve un vélo avec un porte-bagage à l'arrière. Tu peux aussi demander à un·e voisin·e de te le prêter.
4. Mets les smartphones dedans, branchés à la multiprise. Comme ça, les téléphones auront de la batterie pendant un moment.
5. Installe un système de dynamo sur le vélo.
---
6. Branche la multiprise à la dynamo.
7. Installe Google Maps sur tous les appareils.
  + Lance le même itinéraire sur chacun d'entre eux.
8. Fais des allers-retours sur la route principale près de chez toi.
  + N'oublie pas ton casque! La sécurité avant tout.
---
9. Google Maps croit qu'il y a un embouteillage! Les automobilistes vont être dévié·es ailleurs.

