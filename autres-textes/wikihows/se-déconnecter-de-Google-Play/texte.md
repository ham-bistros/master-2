# Comment se déconnecter de Google Play
Google Play est relié à votre compte Google. Pour vous déconnecter sur votre appareil Android, vous devez vous déconnecter de votre compte Google. Sur ordinateur la procédure est beaucoup plus simple.

## Méthode 1 : Se déconnecter de Google Play sur Android
1. Ouvrez les paramètres .
3. Appuyez sur .
4. Comptes
5. Appuyez sur .
6. Google
7. Sélectionnez un compte.
8. Appuyez sur .
9. ⋮
10. Sélectionnez sur .
11. Supprimer le compte
12. Appuyez sur .
13. Supprimer le compte

## Méthode 2 : Se déconnecter de Google Play avec un ordinateur
1. Rendez-vous sur .
2. https://play.google.com
3. Cliquez sur votre photo de profil.
4. Cliquez sur .
5. Se déconnecter
