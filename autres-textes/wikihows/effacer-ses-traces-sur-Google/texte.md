# Comment effacer ses traces sur Google
Vous pouvez supprimer de Google les résultats de recherche indésirables associés à votre nom. En général, le moteur de recherche ne supprime pas les résultats sur une requête, mais il y a plusieurs astuces que vous pouvez essayer pour retirer le contenu sur la page où elle a été publiée. Vous pouvez également utiliser l’outil de suppression de contenu obsolète de Google pour supprimer les versions archivées des contenus déjà supprimés des résultats de recherche.

## Partie 1 : Utiliser des méthodes générales
1. Tapez votre nom sur Google.
2. Sachez quel rôle Google joue là-dedans.
3. Demandez-vous si ça en vaut la peine.
4. Demandez à des amis de supprimer les publications.
5. Modifiez le contenu existant.
6. Supprimez les comptes obsolètes.
7. Soyez proactif.
8. Enterrez le contenu qui vous dérange.

## Partie 2 : Contacter le webmestre
1. Connectez-vous au site web de Whois.
2. Cherchez le site web.
3. Faites défiler la page jusqu’à l’entête ADMINISTRATIVE CONTACT.
4. Passez en revue l’entête Email.
5. Envoyez un message au webmestre.
6. Rédigez une requête professionnelle.
7. Envoyez votre email.
8. Attendez la réponse du webmestre.

## Partie 3 : Supprimer les informations archivées
1. Sachez comment fonctionne cette méthode.
2. Lancez une recherche Google.
3. Cherchez le lien de l’information.
4. Copiez l’adresse du lien.
5. Ouvrez l’outil « Supprimer le contenu obsolète ».
6. Collez le lien de l’adresse.
7. Cliquez sur .
8. DEMANDER LA SUPPRESSION
9. Suivez les instructions fournies.
