#!/usr/bin/env python3

from bs4 import BeautifulSoup
import re
import requests
import sys
import shutil

def dl_img(url, path, index):
    full_url = 'https://www.wikihow.com' + url
    response = requests.get(full_url, stream=True)
    name = re.sub(r'https://.*-728px(.*)', r'\1', full_url)
    print('%s is downloading…' % name)

    with open(path+'/%02d'%i+name, 'wb') as out_file:
        shutil.copyfileobj(response.raw, out_file)
    del response

def get_all_images():
    data = requests.get(url)
    soup = BeautifulSoup(data.text, 'lxml')

    img_tags = soup.select("a.image div img.whcdn")

    for i, tag in enumerate(img_tags):
        link = tag.get('data-srclarge')
        dl_img(link, 'images', '%02d' % i)
