# Comment retrouver la tranquillité ?
Depuis que la grande majorité des gens possède un smarthphone avec un accès à internet, plus personne n'utilise un GPS (et ne parlons pas des cartes routières en papier !). Il n'a jamais été aussi facile de trouver son chemin, et on peut même éviter les embouteillages lorsqu'on est en voiture.  Cela marche parce qu'on peut recouper les données des nombreux·ses utilisateurices  :  là où il y a beaucoup de personnes arrêtées, c'est que le trafic est bloqué ! Il suffit alors d'indiquer aux prochaine·s automobilistes de prendre un autre itinéraire pour éviter le bouchon. Le soucis c'est qu'en faisant ça, on dévie des grandes quantités de voitures sur des routes de campagne qui n'ont pas l'habitude d'avoir autant d'affluence ! Les habitant·es proches de ces zones en ont plus qu'assez de ces allées et venues incessantes. Si tu habites dans ces endroits, voyons comment tu peux arranger ça.

## GUIDE
1. Collecte des smartphones. Demande à ton entourage par exemple !
   + N'oublie pas de leur demander le mot de passe s'il y en a un. Ce serait dommage d'être déjà bloqué·e dans ta démarche.
2. Installe Google Maps sur tous les appareils.
   + Lance le même itinéraire sur chacun d'entre eux.
   + Vérifie que tous indiquent que le trafic est important à l'endroit où tu te trouves.
3. Investis dans des multiprises. Il va falloir garder ces téléphones plein de batterie !
4. Trouve un vélo avec un porte-bagage à l'arrière.
5. Mets les appareils dedans, branchés aux multiprises.
6. Installe un système de dynamo sur le vélo.
7. Branche les prises à la dynamo.
8. Maintenant tu peux te promener tranquillement.
   + N'oublie pas le casque ! La sécurité avant tout.
9. Google croit qu'il y a un embouteillage ! Les automobilistes vont être dévié·es ailleurs.

## RÉACTION
Description : Augmentation importante dans le temps moyen passé dans des conditions de trafic intensif sur la période mars à mai 20XX -- XXXXXXXX, région de XXXXXXXXX
Retours négatifs des utilisateurs : l'itinéraire conseillé semble toujours être le plus long/celui avec le plus de trafic. Une augmentation de 24 min/jour/utilisateur/voiture, soit 12% en comparaison avec les données de la période décembre 20XX - février 20XX.
Les données de trafic ne correspondent pas avec les analyses des données satellites concernant la densité de véhicules visibles par temps clair. Des analyses plus poussées devront être effectuées sur ces non-corrélations.
