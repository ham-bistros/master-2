#!/bin/bash

# # trouve les markdowns
# md=`find CONTENU -type f -name "*.md"`

# # # combine les markdowns
# for m in $md; do sed -e '$s/$/\n\n---\n/' $m ; done > gitlab-pages/mdr.md

# # convertit la fusion en pdf avec LaTeX
# pandoc -s --template -c gitlab-pages/style.css gitlab-pages/template.html --toc --toc-depth=1 --metadata title="avancement" gitlab-pages/mdr.md -o gitlab-pages/site/index.html
# # rm gitlab-pages/mdr.md

rm -r public
mkdir .public

md=`find CONTENU -type f -name "*.md"`
for m in $md; do sed -e '$s/$/\n\n---\n/' $m ; done > gitlab-pages/mdr.md

sed -i 's/<p>\[^\]: /oui/g' gitlab-pages/mdr.md

pandoc -s --template=template.html gitlab-pages/mdr.md -c style.css --toc --toc-depth=1 --metadata title="avancement" -o .public/index.html

# md=`find autres-textes/wikihows -type f -name "*.md"`
# for m in $md; do sed -e '$s/$/\n\n---\n/' $m ; done > gitlab-pages/mdr.md
# pandoc -s --template=template.html gitlab-pages/mdr.md -c style.css --toc --toc-depth=1 --metadata title="\"vrais\" wikihows" -o autres-textes/wikihows/index.html

# cp style.css autres-textes/wikihows/style.css

cp style.css .public//style.css
mv .public public
cp -r cases-bd/fonts public/fonts

# # Génère le pdf
# COUNT=`ls CONTENU/0-PDFs | grep textes | wc -l`
# mv CONTENU/0-PDFs/textes-latest.pdf CONTENU/0-PDFs/textes-v${COUNT}.pdf
# pagedjs-cli public/index.html -o CONTENU/0-PDFs/textes-latest.pdf

