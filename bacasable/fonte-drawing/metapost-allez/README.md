# metapost.survival-kit

## Dépendances
* TeXlive (metapost)
* Python
* FontForge

## Installation

### Tex-Live
#### Linux
 Pour debian/ubuntu : `sudo apt-get install texlive`  
 Pour archlinux/manjaro : `sudo pacman -S texlive`

#### MacOs
Télécharger [macTex](http://www.tug.org/mactex/mactex-download.html). Puis installer le paquet.

#### Windows
Télécharger  le fichier [install-tl-windows.exe](https://www.tug.org/texlive/windows.html), cliquez avec le bouton droit sur le fichier et sélectionnez «Exécuter en tant qu'administrateur»

### Fontforge

Installer Fontforge sur [MacOS](https://fontforge.org/en-US/downloads/mac-dl/)  
Installer Fontforge sur [Windows](https://fontforge.org/en-US/downloads/windows-dl/)  
Installer Fontforge sur linux (debian/ubuntu) `sudo apt-get install fontforge`  
Installer Fontforge sur linux (archlinux/manjaro) `sudo pacman -S fontforge`  

## Arborescence/inventaire du kit de survie


```
├── doc
│   └── 65.svg
├── projet
│   ├── generate.sh  	// générer les svg à partir de base.mp
│   ├── mp
│   │   ├── base.mp  	// les functions et variables globales
│   │   └── glyphs.mp // les descriptions des glyphs
│   └── svg   				// les outputs générer
│       ├── 65.svg
│       ├── 66.svg
│       ├── 67.svg
│       ├── 68.svg
│       ├── 69.svg
│       └── ... 
├── README.md
└── testing
    ├── index.html  	// page web pour visualiser les svg
    ├── main.css
    └── main.js

```


## Exemple de glyph

![A.svg](doc/65.svg)

```
beginchar(65,9);
	x1 := 2 * ux;
	bot y1 := 0 * uy;
	x2 := 4.5 * ux - (strokeX / 4);
	top y2 := capHeight * uy;
	x3 := 4.5 * ux + (strokeX / 4);
	top y3 := capHeight * uy;
	x4 := 7 * ux;
	bot y4 := 0 * uy;
	z5 := .15[z1,z2];
	z6 := .15[z4,z3];

	draw z1 -- z2{dir 20} .. z3 -- z4 withcolor col;
	penTraverse;
	draw z5 -- z6 withcolor col;
endchar(6);

```

Commencer un glyph.
```
beginchar(<keycode letter>,<unité de la chasse>);
...
```

Definir des points.
```
... 
	x1 := 2 * ux;
	y1 := 0 * uy;
...
```

Dessiner un tracé.
```
... 
	draw z1 -- z2 .. z3 -- z4;
...
```
Fermer le glyph.
```
... 
endchar(<nombre de points à rendre visible>);
```
Pour plus de doc aller voir la [cheatsheet.md](https://gitlab.com/erg-type/workshop.meta-elastique/-/blob/master/cheatsheet/cheatsheet.md)

## Ressources Metapost
Des ressources et exemples Metapost en ligne :  
Syracuse doc' : https://melusine.eu.org/syracuse/metapost/mpman/  
Metapost raconté aux piétons, Yves Soulet - cahiers GUTenberg, n°52-53, octobre 2009 http://yves.soulet.free.fr/pub/manuel_pour_metapost_2009.pdf  
The METAFONT tutorial, ChristopheGrandsire - Version 0.33, December 30, 2004 http://metafont.tutorial.free.fr/downloads/mftut.pdf  





