# Comment devenir un hacker
Il existe un monde, celui de la culture partagée, qui est composé de programmateurs, d'experts et d'assistants réseau. C'est un milieu dont on retrouve la trace sur des décennies à commencer par les premiers microordinateurs en réseau et les premières expérimentations sur l'ARPANET. Les tenants de cette culture se sont donné le nom de « hackers ». Beaucoup estiment que les hackers sont simplement des gens qui s'introduisent dans les ordinateurs et font du piratage téléphonique, mais être hacker ce n'est pas ça, c'est une vraie culture avec de vrais principes que bien souvent le public méconnait. Apprenez les techniques de base du hacking, comment penser « hacker » et comment se faire un nom dans ce milieu.

## Partie 1 : Apprendre les techniques de base du hacking
1. Récupérez un Unix en open source et apprenez à l'utiliser et à le faire tourner.
2. Apprenez à écrire en HTML.
3. Apprenez à programmer.

## Partie 2 : Penser comme un hacker
1. Soyez créatif.
2. Apprenez à résoudre des problèmes
3. .
4. Reconnaissez et luttez contre l'autorité !
5. Pour être hacker, il faut en avoir les compétences.

## Partie 3 : Gagner le respect de la communauté
1. Écrivez un logiciel open source.
2. Testez et déboguez les logiciels open source.
3. Publiez des informations utiles.
4. Travaillez à l'infrastructure.
5. Soyez au service de la culture hacker.
