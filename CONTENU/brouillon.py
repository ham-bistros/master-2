#!/usr/bin/env python3

def markdown_magic(md_file):
    with open(md_file, 'r', encoding='utf-8') as f:
        md = f.read().splitlines()

    final= []

    md = [m.strip() for m in md]
    contenu = list(filter(None, md))

    for line in contenu:
        print("'%s'" % line)
        if line[0:2] == '# ':
            final.append(('h1', line[2:]))
        elif line[0:2] == '##':
            final.append(('h2', line[4:]))
        elif line[0] in '0123456789':
            final.append(('section', line))
        else:
            final.append(('p', line))

    for fin in final:
        print(fin)

    # return contenu

markdown_magic('1-detournement-gmaps/texte.md')
# markdown_magic('1-reconnaissance-vocale/texte.md')

