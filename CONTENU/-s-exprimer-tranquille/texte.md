- **ALGORITHME**
- **LOGICIEL**
- **WEB**
- **NLP**
- **ROBOT**

L'anglais est l'une des langues les plus parlée dans le monde, et elle est aussi très présente sur le web. Cependant, puisqu'elle est très parlée elle est aussi facilement décryptée par des logiciels d'analyse de texte. Afin d'éviter cela, nous allons voir comment saboter ces algorithmes de compréhension du langage.

### Utilise une langue peu connue
Même si l'anglais est majoritaire, le français fait partie des 10 langues les plus utilisées sur le web. Il faut trouver quelque chose d'un peu plus original!

1. Choisis une langue construite[^]. De préférence, elle doit être peu parlée.
   + Ce que tu peux exprimer dépend beaucoup de la langue!
2. Étudie cette langue en profondeur.
3. Enseigne-là autour de toi! Il faut savoir partager.
5. Tu peux changer de langue régulièrement pour rester frai·s·che.

### Utilise une autre façon de parler

Les algorithmes de **NLP** peuvent reconnaître le ton d'un texte, et donc savoir si on exprime une opinion positive ou négative d'un certain sujet.

1. Ne dis jamais ce que tu penses de manière claire.
2. Utilise des périphrases si tu dois parler d'une personne ou d'un lieu en particulier.
3. Fais des blagues! Les robots ne comprennent pas l'humour.
4. Ait l'air enjoué·e pour parler d'un sujet triste ou énervant, et inversement.

