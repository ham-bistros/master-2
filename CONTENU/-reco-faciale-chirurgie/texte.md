- **DONNÉES**
- **MÉTADONNÉES**
- **RECONNAISSANCE FACIALE**
- **INTELLIGENCE ARTIFICIELLE**

Quand on est tête en l'air et qu'on oublie tout le temps son mot de passe, rien de tel que la reconnaissance faciale pour déverrouiller son téléphone ou son compte en banque. Ça peut aussi être bien utile pour repérer rapidement les criminel·les dans les gares ou les aéroports!
Mais en réfléchissant à la protection des données personnelles et aux dérives de la surveillance de masse, tu pourrais avoir envie de saboter cette technologie. C'est ce qu'on va voir maintenant!

### Chirurgie
1. Travaille bien à l'école.
2. Dirige-toi vers la médecine. Tes parents vont être fièr·es de toi!
3. Spécialise-toi dans la chirurgie esthétique.
4. Deviens célèbre dans le milieu. Plein de gens vont être intéressé·es par tes services!
5. Tire parti de leurs complexes causés par Instagram.
6. Retouche leur visage.
---
7. Certains aspects du visage sont cruciaux pour la reconnaissance faciale:
   + La forme et la position du nez (comme le nez au milieu de la figure!)
   + L'écart entre les deux yeux
   + La distance entre le front et le menton
8. Il faut que ce soit suffisament prononcé pour que les logiciels soient perturbés.
9. N'oublie pas, il faut souffrir pour être belleau.
