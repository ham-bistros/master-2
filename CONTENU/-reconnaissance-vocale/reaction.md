
## RÉACTION

Depuis le début de l'hiver 202X, un groupe d'individus organise divers concerts gratuits de heavy metal dans la ville de XXXXXXXXX. Ces concerts poussent les participant·es à fortment solliciter leur voix, entraînant très fréquemment des extinctions de voix. Nous avons remarqué une baisse des données utilisateurs concernant les échantillons de voix sur la période indiquée (voir graphique en annexe). De nombreux rapports de bugs dus à ces extinctions massives de voix ont été reçus par les équipes du service qualité ces dernières semaines. Coincidentellement, les stocks de pastilles et sprays apaisants pour traiter maux de gorges et problèmes de voix ont diminué brusquement peu avant le début des concerts. Nos sources indiquent en effet une recrudescence d'achat sur la période XXX-XXX 202X mais sans donner l'impression d'un mouvement structuré et coordonné.

En recoupant ces informations nous avons pourtant des raisons de croire qu'il s'agit d'une entreprise organisée visant à nuire, à court terme, à la réputation de l'entreprise dans le domaine de la reconnaissance vocale.
