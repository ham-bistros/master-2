# Comment inventer une langue
Que ce soit le klingon dans l'univers de Star Trek ou le na'vi dans le film « Avatar » de James Cameron, les langues fictionnelles permettent de donner plus de réalité à une œuvre de fiction. La création d'une langue peut être une entreprise plutôt intense, car le processus est complexe et demande beaucoup de réflexions. Cependant, avec de la pratique et du dévouement, tout le monde peut créer sa propre langue pour s'amuser ou pour compléter un monde imaginaire.

## Partie 1 : Construire le vocabulaire
1. Identifiez les mots simples pour des phrases de base.
2. Trouvez des mots pour des objets de la vie quotidienne.
3. Créez votre propre dictionnaire depuis votre langue maternelle.
4. Combinez des mots simples pour créer des mots composés.
5. Donnez un nom à votre langue.

## Partie 2 : Écrire des mots et des phrases
1. Créez votre propre alphabet pour écrire votre langue.
2. Empruntez des lettres à un alphabet existant.
3. Utilisez des pictogrammes ou des symboles pour les mots.
4. Ajoutez des accents pour créer de nouvelles lettres.

## Partie 3 : Former des phrases simples
1. Choisissez le bon ordre de phrase.
2. Décidez si vous voulez des substantifs pluriels.
3. Réfléchissez à l'utilisation des verbes.
4. Entrainez-vous à la parler et à l'écrire.
