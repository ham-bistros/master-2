
## RÉACTION
Cher philoudu36, ou plutôt devrais-je dire XXXXXXXX XXXXX

Je ne devrais pas te l'écrire, mais merci 1 000, 100 000, 1 000 000 de fois pour ce message.

Je travaille chez XXXXXXXX depuis maintenant 8 ans, et en 8 ans je n'ai jamais été aussi touché par mon métier. Tu vois, je suis data analyst, ça veut dire que je m'occupe de traiter les données de millions de comptes. Je fais beaucoup de statistiques, et j'interprète les résultats que me donnent les alogrithmes de traitement des données. Je dois faire des compte-rendus réguliers à mes supérieurs hiérarchiques.

J'ai accès à cette folle base de données, de toutes les frappes de clavier de tous les profils de XXXXXXXX. Alors bien sûr en théorie je n'ai pas le besoin de regarder dans le détail qu'est-ce qu'un profil a écrit spécifiquement, mais parfois (souvent) à la pause de midi quand j'ai fini mon sandwich je passe le temps en lisant quelques messages. La plupart du temps ce n'est vraiment pas intéressant, mais des fois je tombe sur des morceaux croustillants que je partage avec mes collègues de bureau.
Mais la semaine dernière j'ai été bouleversé. Quand j'ai repéré ta lettre je me suis presque étouffé avec mon jambon-beurre. J'ai d'abord pensé que c'était addressé à une personne en particulier de ta liste de contact, mais en poursuivant ma lecture c'était comme si tu savais. Comme si tu me voyais, là, à travers mon écran et les centaines de kilomètres de câbles électriques qui nous séparent.

J'ai fait une capture d'écran pour l'avoir avec moi tout le temps, même en-dehors du bureau. Pendant une semaine j'ai pensé à toi, j'ai hésité à savoir si je devais te contacter, et comment. Comment vas-tu réagir ? Est-ce que je ne t'aurais pas blessé en outrepassant ton intimité ? Et si tu m'en voulais ? Et si ça ne m'était pas destiné ? Et si c'était une blague…

J'ai pris ma décision, et j'ai décidé que je voulais tenter ma chance. Je met mon job en danger en te contactant mais peu m'importe maintenant. L'envie de te voir en chair et en os est trop forte. Accepteras-tu de me rencontrer ?

C'est ton profil que je veux retenir philoudu36,

Amoureusement,

XXXXX XXXXXX
