from jinja2 import Environment, FileSystemLoader
from distutils.dir_util import copy_tree
import csv
import os
import pyinotify
import asyncio
import re

import halftone as ht
from PIL import Image, ImageEnhance
import subprocess

def markdown_magic(md_file):
# parse le markdown pour le mettre dans le template Jinja
    with open(md_file, 'r', encoding='utf-8') as f:
        base = f.read()
        base = re.sub(r'\*\*(.*)\*\*', r'<strong>\1</strong>', base)
        md = re.sub(r'([:;!?])', r'&thinsp;\1', base).splitlines()

    final= []

    md = [m.strip() for m in md]
    contenu = list(filter(None, md))
    les_plus = {}

    for i, line in enumerate(contenu):
        if line[0:2] == '##':
            final.append(('h2', line[3:]))

        elif line == '---':
            final.append(('---', '<br>'))

        elif line[0] in '0123456789':
            if i + 1 < len(contenu) and contenu[i+1][0] == '+':
                final.append(('div', line))
                nb_plus = 0

                for r in range(i, len(contenu)):
                    if contenu[r+1][0].lstrip() == '+':
                        nb_plus += 1
                    else:
                        break

                les_plus[i] = nb_plus

            else:
                final.append(('section', re.sub('^.*?\. ', '', line)))
                # final.append(('section', line))

        elif line[0] == '+':
            if i + 1 < len(contenu) and contenu[i+1][0] == '+':
                final.append(('prec', line))

            else:
                final.append(('preclast', line))

        elif line[0] == '[':
            final.append(('blockquote', line))

        else:
            final.append(('p', line))

    return final, les_plus

def traming(images, dir, parsed, les_plus):
# convertit les images en tramasse em fonction de leur taillasse
    tagz = ['section', 'div', 'prec', 'preclast']
    compteur = 0
    coef = []

    # for i, (tag, line)in enumerate(parsed):
        # if tag in tagz:
        #     if i in les_plus:
        #         nb_plus = les_plus[i]

        #         if nb_plus == 1:
        #             coef.append(.5)
        #             # print(images[compteur].split('/')[-1], .5)
        #             compteur += 1
        #             coef.append(.5) # pour la ligne + juste après
        #             # print(images[compteur].split('/')[-1], .5) # pour la ligne + juste après
        #             compteur += 1
        #         else:
        #             coef.append(0.33333 * nb_plus)
        #             # print(images[compteur].split('/')[-1], 0.33333 * nb_plus)
        #             compteur += 1
        #             for r in range(1, nb_plus + 1):
        #                 coef.append(0.33333)
        #                 compteur += 1
        #                 # print(images[compteur].split('/')[-1], 0.33333)

        #     else:
        #         if line[0] == '+':
        #             continue
        #         elif i+1 in les_plus and les_plus[i+1] == 1 and i%2 == 0:
        #             coef.append(1)
        #             # print(images[compteur].split('/')[-1], 1)
        #             compteur += 1
        #         else:
        #             if images[compteur].split('/')[-1].split('/')[-1].split('-')[1] == '*':
        #                 coef.append(1)
        #                 # print(images[compteur].split('/')[-1], 1)
        #                 compteur += 1
        #             else:
        #                 coef.append(.5)
        #                 # print(images[compteur].split('/')[-1], .5)
        #                 compteur += 1

    # if not os.path.isdir('%s/images-halftoned/' % dir):
    #     os.mkdir('%s/images-halftoned/' % dir)
    #     demi_teinte = True
    # else:
    #     print('Les images sont déjà tramées')
    #     demi_teinte = False

    # if not os.path.isdir('%s/images-vecto/' % dir):
    #     os.mkdir('%s/images-vecto/' % dir)
    #     demi_teinte = True
    # else:
    #     print('Les images sont déjà vectorisées')
    #     demi_teinte = False

    n_et_b = True
    demi_teinte = True
    vecto = True

    # coef = [.33,.5,1,2,3,4]
    # coef = [.33,.5,1,2,3,4]

    for i, im in enumerate(images):
        nvl_image = '%s/images-nb/%s' % (dir, im.split('/')[-1])

        if n_et_b:
            img = Image.open(im[3:]).convert('L')
            enhancer = ImageEnhance.Contrast(img)

            nb_img = enhancer.enhance(2)
            nb_img.save(nvl_image)

        if demi_teinte:
            # angle = 0
            angle = 45

            # spacing = 10 / coef[i]
            spacing = 10

            imgo = Image.open(nvl_image)
            print('%s/images-nb/%s' % (dir, im.split('/')[-1]))
            rot_img = imgo.transpose(Image.ROTATE_90)

            halftoned = ht.halftone(rot_img, ht.line(angle=angle, spacing=spacing))

            out = halftoned.transpose(Image.ROTATE_270)
            out.save('%s/images-halftoned/%s.pgm' % (dir, im.split('/')[-1][:-4]))

    if vecto:
        commande = '/usr/bin/bash autotracing.sh %s/images-halftoned' % dir
        subprocess.run(commande.split(' '))

    # return coef

### ------------------ CÉPARTI ----------------- ###
class StaticSite:

    def __init__(self):
        self.templatesDir = 'templates/'
        self.defaultTemplateFile = 'index.html'
        self.siteDir = 'site/'
        self.statics = 'statics'
        self.port = 8888
        self.dirs = {}
        self.noms_dossiers = []

        for f in os.listdir('.'):
            if os.path.isdir(f) and f[0] == '1':
                self.noms_dossiers.append(f)
                self.dirs[f] = os.listdir(f)

    def templateToSite(self):
        file_loader = FileSystemLoader('templates')
        env = Environment(loader=file_loader)
        template = env.get_template('main.html')
        output = template.render(contenu=self.dirs)

        with open(self.siteDir+'index.html', "w") as fh:
            fh.write(output)

        guide_template = env.get_template('guide.html')
        for j, dir in enumerate(self.dirs):
            print('\n' + dir)
            parsed, les_plus = markdown_magic('%s/texte.md' % dir)

            img_paths = []
            svg_text = []
            for img in os.listdir('%s/images' % dir):
                if img[0] in '0123456879':
                    img_paths.append('../%s/images/%s' % (dir, img))

            if j == 0:
                prec = ''
            else:
                prec = self.noms_dossiers[j-1]

            if j == len(self.noms_dossiers) - 1:
                suiv = ''
            else:
                suiv = self.noms_dossiers[j+1]

            # coef = traming(img_paths, dir, parsed, les_plus)
            traming(img_paths, dir, parsed, les_plus)

            for svg in os.listdir('%s/images-vecto' % dir):
                # print(svg[:20])
                with open('%s/images-vecto/%s' % (dir, svg)) as f:
                    svg_text.append(f.read())

            out = guide_template.render(img_paths=img_paths, parsed=parsed, prec=prec, suiv=suiv, les_plus=les_plus, svg_text=svg_text)

            with open('%s/index.html' % dir, "w") as f:
                f.write(out)

        copy_tree('templates/statics/', 'site/')

    def build(self):
        self.templateToSite()

    def watch(self):

        self.build()
        self.templateToSite()

        def handle_read_callback(event):
            self.templateToSite()
            print('------->    ', event.process_events)
            print(dir(event))

        wm = pyinotify.WatchManager()
        loop = asyncio.get_event_loop()
        notifier = pyinotify.AsyncioNotifier(wm, loop,
                                             callback=handle_read_callback)
        wm.add_watch('templates/', pyinotify.IN_MODIFY)
        loop.run_forever()
        notifier.stop()


#     class Serve:
#         def run(self):
#             self.build()
#             self.templateToSite()
#             from http.server import HTTPServer, CGIHTTPRequestHandler
#             import threading

#             def start_server(path, port=8000):
#                 '''Start a simple webserver serving path on port'''
#                 os.chdir(path)
#                 httpd = HTTPServer(('', port), CGIHTTPRequestHandler)
#                 httpd.serve_forever()

#             port = 8000
#             daemon = threading.Thread(name='daemon_server',
#             target=start_server,
#             args=('site/', port))
#             daemon.setDaemon(False) # Set as a daemon so it will be killed once the main thread is dead.
#             daemon.start()
