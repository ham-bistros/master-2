## Bris Thomas - Typographie

# Comment tester une résistance

Alors ce projet c'est un genre de manuel de désobéissance civile contre la numérisation de la société. C'est né d'un mélange entre l'envie d'informer les gens sur les problèmes que le numérique (et son omniprésence) pose, l'envie d'aider les gens qui galèrent avec les ordinateurs et l'informatique dans leur vie quotidienne et la non-envie de faciliter l'avènement du techno-capitalisme. C'est un manuel donc ce qui est dedans est réalisable mais parfois assez exagéré.
