#/bin/sh

pandoc --css=style.css --template=template.html -s -f markdown+smart --metadata title="de gros doutes" --to=html5 doutes.md -o index.html

# Ajout de classes
sed -i 's/^<p>$ /<p class="existentiel">/g' index.html
sed -i 's/^<p>µ /<p class="technique">/g' index.html
sed -i 's/^<p>| /<p class="theorique">/g' index.html
sed -i 's/^<p>&amp; /<p class="faire">/g' index.html
sed -i 's/^<p>%/<p class="reference">/g' index.html
sed -i 's/-&gt;/→/g' index.html
sed -i 's/ \([?;:!]\)/\1/g' index.html
sed -i 's/\([?;:!]\)/\&thinsp;\1/g' index.html
sed -i 's/<&thinsp;!DOCTYPE html>/<!DOCTYPE html>/g' index.html
