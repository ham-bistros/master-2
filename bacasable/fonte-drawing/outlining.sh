#!/bin/sh

sed -i "s/<path fill=".*"/<path fill=\"#ffffff\" stroke=\"#000000\"/g" outlined/*.svg

mogrify -format png outlined/*.svg
rm outlined/*.svg
