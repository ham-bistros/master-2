- **DONNÉES**
- **MÉTADONNÉES**
- **ROBOT**
- **LOGICIEL**
- **SITE WEB**
- **PLATEFORME**

Quand on est sur un ordinateur, on a souvent besoin d'écrire du texte à l'aide d'un clavier. Certains logiciels ou certains sites que l'on utilise peuvent enregistrer nos frappes de clavier afin d'améliorer leurs services de correction orthographique ou encore d'autocomplétion des mots. Tout ce qui est tapé peut alors être analysé par des robots. Si toi aussi tu préfèrerais que ce qui soit analysé soit quelque chose de beau et de positif, c'est par ici que ça se passe!

### Lettre d'amour
Si tu es en manque de romantisme, écrit une lettre! Ça peut avoir l'air vieux jeu, mais de nos jours c'est original.
1. Fais une première version sur papier.
  + Il faut que ce soit parfait du premier coup, pas de ratures ou d'hésitation quand tu taperas ton message!
2. Réfléchis à ce que tu ressens.
3. Cherche des exemples. Regarde des films ou bien lis des romans romantiques pour t'inspirer.
4. Prends ton temps. Il ne faut rien presser en amour.
5. C'est le moment d'écrire! Prends ton courage à deux mains et laisse parler ton cœur.
6. N'écris pas spécifiquement pour une personne en particulier, tu ne sais pas qui peux te lire.
7. Relis-toi. C'est important de ne pas faire de faute d'orthographe.
8. Quand tu es sûr·e de toi, recopie la lettre sur un ordinateur {trouver un exemple de logiciel/plateforme ?}.
9. Applique-toi!
10. Quand tu as fini, efface tout.
11. Quelqu'un·e, quelque part, va peut-être lire cette lettre, dans un océan de données.
12. Même si tu n'as pas de réponse, sois content·e de ta bonne action.
13. C'est un peu comme écrire à un·e admirateurice secret·e, mais l'inverse.

