# Comment supprimer un compte Google ou Gmail
Si pour une raison ou une autre vous ne voulez plus utiliser votre compte Google ou Gmail, vous pouvez le supprimer en quelques clics depuis n'importe quel navigateur web. Sachez cependant que la suppression de votre compte Google supprimera toutes vos données Google et la suppression de votre Gmail supprimera votre adresse ainsi que vos données.

## Méthode 1 : Supprimer un compte Google
1. Tapez <samp style="padding-left:0.2em; padding-right:0.1em; color: #666666;">myaccount.google.com</samp> dans votre navigateur.
2. Cliquez sur Connexion si vous n'êtes pas encore connecté.
3. Connectez-vous avec le compte que vous voulez supprimer.
4. Sélectionnez Supprimer votre compte ou des services.
5. Cliquez sur Supprimer le compte Google et les données associées.
6. Entrez une nouvelle fois votre mot de passe.
7. Passez en revue le contenu qui sera supprimé.
8. Sélectionnez Télécharger vos données.
9. Faites défiler vers le bas et cochez les 2 cases Oui.
10. Cliquez sur Supprimer le compte.
11. Essayez de récupérer le compte supprimé.

## Méthode 2 : Supprimer une adresse Gmail
1. Tapez <samp style="padding-left:0.2em; padding-right:0.1em; color: #666666;">myaccount.google.com</samp> dans votre navigateur.
2. Sélectionnez Connexion.
3. Connectez-vous au compte Gmail à supprimer.
4. Cliquez sur Supprimer votre compte ou des services.
5. Sélectionnez Supprimer des produits.
6. Entrez une nouvelle fois votre mot de passe Gmail.
7. Cliquez sur Supprimer à côté de Gmail.
8. Tapez une autre adresse email pour votre compte Google.
9. Sélectionnez Envoyez un email de validation.
10. Connectez-vous à l'autre compte de messagerie.
11. Ouvrez l'email de validation envoyé par Google.
12. Cliquez sur le lien dans l'email.
