# Comment aller sur Google en Chine
Vous devez utiliser un réseau privé virtuel (Virtual Private Network ou VPN en anglais) pour accéder au site web de Google depuis la Chine, car il n'existe aucune solution officielle ou « légale » pour le faire. Un VPN est un programme qui vous permet de faire croire que vous vous connectez à Internet depuis un autre endroit, différent de votre emplacement réel. Dans ce cas, vous pouvez faire croire que vous êtes en train d'accéder au site web Google depuis les États-Unis, où il n'est pas banni, même si vous vous trouvez réellement en Chine, où c'est malheureusement le cas.

## Partie 1 : Comprendre les VPN
1. Téléchargez un VPN pour dissimuler votre adresse IP.
2. Comprenez que l'Internet chinois ne va pas renvoyer de résultats occidentaux.
3. Sachez qu'il est techniquement légal d'utiliser un VPN.

## Partie 2 : Choisir un VPN
1. Étudiez cette liste des VPN les plus connus.
2. Comprenez que les VPN ne sont pas éternels.
3. Gardez à l'esprit que certains sites web chinois domestiques ne seront pas accessibles à cause du VPN.

## Partie 3 : Utiliser un VPN
1. Utilisez un protocole VPN.
2. Téléchargez et installez le VPN.
3. Lancez le programme de VPN.
4. Sélectionnez le pays avec lequel vous voulez masquer votre adresse IP.
5. Assurez-vous que vous avez une connexion haut débit.
6. Faites attention si vous allez sur Google en Chine, même avec un VPN.
