## RÉACTION
Augmentation importante dans le temps moyen passé dans des conditions de trafic intensif sur la période mars à mai 20XX -- XXXXXXXX, région de XXXXXXXXX

### SECTION D136-D138A

Trafic moyen annuel: 240 véhicules-kilomètre
Vitesse moyenne: 45.8 km/h -> -32%

Répartition hebdomadaire moyenne (en véhicules-kilomètre):

  LUNDI  |  MARDI  |  MERCREDI  |  JEUDI  |  VENDREDI  |  SAMEDI  |  DIMANCHE  |
---------|---------|------------|---------|------------|----------|------------|
   532   |   498   |    489     |   503   |     507    |    67    |     80     |
         |         |            |         |            |          |            |


