#!/bin/bash

fonte=$1

dirname="${1%%.*}-traced/"
mkdir -p $dirname/tmp
mkdir -p $dirname/

python exportor.py $1
mogrify -format pbm $dirname/tmp/*.svg

for file in `find $dirname/tmp -type f -name "*.pbm"`;
do
  fname=${file##*/}
  autotrace -centerline $file -output-file ${fname%.*}svg;
done

rm -rf $dirname/tmp

