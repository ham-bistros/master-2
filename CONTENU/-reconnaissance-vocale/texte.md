- **DONNÉES/MÉTADONNÉES**
- **RECONNAISSANCE VOCALE**
- **LOGICIEL**
- **INTELLIGENCE ARTIFICIELLE**

L’assistance vocale (comme Siri ou Alexa) permet d’utiliser son téléphone ou son ordinateur avec seulement sa voix, en gardant les mains libres. Sympa! Cependant, pour que ces logiciels s’activent quand on prononce leur nom, ils doivent écouter en permanence ce qui se passe autour. Comment faire pour empêcher plein de monde d'être espionné·es à leur insu?

## En organisant des soirées

Cette méthode demande un peu de préparation, mais peut-être amusante et même devenir un métier!

1. Rachète tous les stocks de pastilles pour la gorge. C'est un investissement de départ, mais augmentera l'impact de ton action à moyen terme.
2. Organise des concerts gratuits. Le but est que les gens crient un maximum, alors essaie de contacter des groupes suffisamment connus pour attirer du monde.
   + Évite les groupes du musique instrumentale, car les gens n'auront pas l'occasion de se casser la voix en chantant. Tu peux jouer sur la nostalgie ou bien opter pour des styles énergiques et vocaux, comme le *death metal* par exemple.
3. Profite de la saison! En organisant les concerts en hiver, de préférence en extérieur, les gens vont plus facilement attraper froid, avoir un rhume ou un mal de gorge naturellement.
4. Si tout le monde s'amuse bien, beaucoup de personnes auront une extinction de voix dans les jours suivants. Siri ne comprend plus rien maintenant.
---
5. Passe à l'international. Si le modèle fonctionne-bien, exporte-le! Ça sera utile pour diffuser ton message.

