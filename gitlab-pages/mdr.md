# Comment retrouver la tranquillité ?

- algo de recommandation
- SaaS ?

Les GPS modernes permettent facilement de contourner les embouteillages ou les travaux en temps réel. Pour ça ils peuvent indiquer les itinéraires les moins fréquentés, et donc conseiller des routes qui étaient d'habitude désertes. Si tu habites près d'une de ces routes, tu es sûrement dérangé·e par les voitures, surtout le weekend! Voyons comment tu peux retrouver le calme en empêchant les GPS de faire tout le temps passer les voitures par chez toi.

### Méthode 1 : Fais-croire au GPS qu'il y a un embouteillage
1. Emprunte des smartphones à tes ami·es et voisin·es. Essaie d'en avoir le plus possible.
  + Préviens-les que ce ne sera pas long! Seulement le samedi après-midi par exemple.
  + N'oublie pas de leur demander le mot de passe s'il y en a un.
2. Investis dans une grosse multiprise.
3. Trouve un vélo avec un porte-bagage à l'arrière. Tu peux aussi demander à un·e voisin·e de te le prêter.
4. Mets les smartphones dedans, branchés à la multiprise. Comme ça, les téléphones auront de la batterie pendant un moment.
5. Installe un système de dynamo sur le vélo.
6. Branche la multiprise à la dynamo.
7. Installe Google Maps sur tous les appareils.
  + Lance le même itinéraire sur chacun d'entre eux.
8. Fais des allers-retours sur la route principale près de chez toi.
  + N'oublie pas ton casque! La sécurité avant tout.
9. Google Maps croit qu'il y a un embouteillage! Les automobilistes vont être dévié·es ailleurs.

## RÉACTION
Augmentation importante dans le temps moyen passé dans des conditions de trafic intensif sur la période mars à mai 20XX -- XXXXXXXX, région de XXXXXXXXX

### SECTION D136-D138A

Trafic moyen annuel: 240 véhicules-kilomètre
Vitesse moyenne: 45.8 km/h -> -32%

Répartition hebdomadaire moyenne (en véhicules-kilomètre):

  LUNDI  |  MARDI  |  MERCREDI  |  JEUDI  |  VENDREDI  |  SAMEDI  |  DIMANCHE  |
---------|---------|------------|---------|------------|----------|------------|
   532   |   498   |    489     |   503   |     507    |    67    |     80     |
         |         |            |         |            |          |            |


---

# Comment raviver la flamme ?

[^]: Les **DONNÉES** c'est plein de trucs, c'est juste des "informations". Mais ça peut être du texte, des images, des vidéos, du son, des fichiers… Ce qu'on s'échange par internet quoi, ou ce qui compose les sites. Ça peut aussi être des infos en plus ou des statistiques, et là c'est plus des métadonnées.

[^]: Les **MÉTADONNÉES** c'est des données hein, mais plus ce qu'il y a "autour" de la donnée principale. Par exemple t'envoie un mail à un·e pote et tu lui écris "salut". La donnée principale que t'envoie c'est le message, c'est "salut". Les métadonnées c'est l'adresse mail de ta·on pote, la tienne, le jour et l'heure à laquelle t'envoies le mail, l'endroit depuis où tu l'envoies, avec quel appareil/ordi tu l'envoies, des trucs du style quoi. Ça à l'air pas important mais tu peux analyser pleins de choses avec des métadonnées, plus t'en as plus tu peux faire des stats, les comparer, les croiser pour trouver ou déduire d'autres infos. Ces autres infos c'est aussi des données, qui ont aussi des métadonnées, et là ça devient le bordel, c'est la fête aux données dans tous les sens, c'est le Big Data.

[^]: Quand on parle de **ROBOT** c'est assez vague commme terme mais tu peux imaginer une machine qui exécute des actions programmées à l'avance. C'est pas seulement une machine mécanique, ça peut aussi être une machine numérique (donc un algorithme). Programme, robot et algorithme c'est plus ou moins des synonymes.

- logiciel
- site web
- plateforme

Quand on est sur un ordinateur, on a souvent besoin d'écrire du texte à l'aide d'un clavier. Certains logiciels ou certains sites que l'on utilise peuvent enregistrer nos frappes de clavier afin d'améliorer leurs services de correction orthographique ou encore d'autocomplétion des mots. Tout ce qui est tapé peut alors être analysé par des robots. Si toi aussi tu préfèrerais que ce qui soit analysé soit quelque chose de beau et de positif, c'est par ici que ça se passe !

### Lettre d'amour
Si tu es en manque de romantisme, écrit une lettre! Ça peut avoir l'air vieux jeu, mais de nos jours c'est original.
1. Commence sur papier.
  + Il faut que ce soit parfait du premier coup, alors pas de ratures ou d'hésitation quand tu taperas ton message.
2. Réfléchis à ce que tu ressens.
3. Cherche des exemples. Regarde des films ou bien lis des romans romantiques pour t'inspirer.
4. Prends ton temps. Il ne faut rien presser en amour.
5. C'est le moment d'écrire ! Prends ton courage à deux mains et laisse parler ton cœur.
6. N'écris pas spécifiquement pour une personne en particulier, tu ne sais pas qui peux te lire.
7. Relis-toi. C'est important de ne pas faire de faute d'orthographe.
8. Quand tu es sûr·e de toi, recopie la lettre sur un ordinateur {trouver un exemple de logiciel/plateforme ?}.
9. Applique-toi !
10. Quand tu as fini, efface tout.
11. Quelqu'un·e, quelque part, va peut-être lire cette lettre, dans un océan de données.
12. Même si tu n'as pas de réponse, sois content·e de ta bonne action.
13. C'est un peu comme écrire à un·e admirateurice secret·e, mais l'inverse.

## RÉACTION
Cher philoudu36, ou plutôt devrais-je dire XXXXXXXX XXXXX

Je ne devrais pas te l'écrire, mais merci 1 000, 100 000, 1 000 000 de fois pour ce message.

Je travaille chez XXXXXXXX depuis maintenant 8 ans, et en 8 ans je n'ai jamais été aussi touché par mon métier. Tu vois, je suis data analyst, ça veut dire que je m'occupe de traiter les données de millions de comptes. Je fais beaucoup de statistiques, et j'interprète les résultats que me donnent les alogrithmes de traitement des données. Je dois faire des compte-rendus réguliers à mes supérieurs hiérarchiques.

J'ai accès à cette folle base de données, de toutes les frappes de clavier de tous les profils de XXXXXXXX. Alors bien sûr en théorie je n'ai pas le besoin de regarder dans le détail qu'est-ce qu'un profil a écrit spécifiquement, mais parfois (souvent) à la pause de midi quand j'ai fini mon sandwich je passe le temps en lisant quelques messages. La plupart du temps ce n'est vraiment pas intéressant, mais des fois je tombe sur des morceaux croustillants que je partage avec mes collègues de bureau.
Mais la semaine dernière j'ai été bouleversé. Quand j'ai repéré ta lettre je me suis presque étouffé avec mon jambon-beurre. J'ai d'abord pensé que c'était addressé à une personne en particulier de ta liste de contact, mais en poursuivant ma lecture c'était comme si tu savais. Comme si tu me voyais, là, à travers mon écran et les centaines de kilomètres de câbles électriques qui nous séparent.

J'ai fait une capture d'écran pour l'avoir avec moi tout le temps, même en-dehors du bureau. Pendant une semaine j'ai pensé à toi, j'ai hésité à savoir si je devais te contacter, et comment. Comment vas-tu réagir ? Est-ce que je ne t'aurais pas blessé en outrepassant ton intimité ? Et si tu m'en voulais ? Et si ça ne m'était pas destiné ? Et si c'était une blague…

J'ai pris ma décision, et j'ai décidé que je voulais tenter ma chance. Je met mon job en danger en te contactant mais peu m'importe maintenant. L'envie de te voir en chair et en os est trop forte. Accepteras-tu de me rencontrer ?

C'est ton profil que je veux retenir philoudu36,

Amoureusement,

XXXXX XXXXXX

---

# Comment parasiter la reconnaissance faciale ?

[^]: Les **DONNÉES** c'est plein de trucs, c'est juste des "informations". Mais ça peut être du texte, des images, des vidéos, du son, des fichiers… Ce qu'on s'échange par internet quoi, ou ce qui compose les sites. Ça peut aussi être des infos en plus ou des statistiques, et là c'est plus des métadonnées.

[^]: Les **MÉTADONNÉES** c'est des données hein, mais plus ce qu'il y a "autour" de la donnée principale. Par exemple t'envoie un mail à un·e pote et tu lui écris "salut". La donnée principale que t'envoie c'est le message, c'est "salut". Les métadonnées c'est l'adresse mail de ta·on pote, la tienne, le jour et l'heure à laquelle t'envoies le mail, l'endroit depuis où tu l'envoies, avec quel appareil/ordi tu l'envoies, des trucs du style quoi. Ça à l'air pas important mais tu peux analyser pleins de choses avec des métadonnées, plus t'en as plus tu peux faire des stats, les comparer, les croiser pour trouver ou déduire d'autres infos. Ces autres infos c'est aussi des données, qui ont aussi des métadonnées, et là ça devient le bordel, c'est la fête aux données dans tous les sens, c'est le Big Data.

- reconnaissance faciale
- intelligence artificielle

Quand on est tête en l'air et qu'on oublie tout le temps son mot de passe, rien de tel que la reconnaissance faciale pour déverrouiller son téléphone ou son compte en banque. Ça peut aussi être bien utile pour repérer rapidement les criminel·les dans les gares ou les aéroports!
Mais en réfléchissant à la protection des données personnelles et aux dérives de la surveillance de masse, tu pourrais avoir envie de saboter cette technologie. C'est ce qu'on va voir maintenant!

### Méthode 1:
1. Travaille bien à l'école.
2. Dirige-toi vers la médecine. Tes parents vont être fièr·es de toi!
3. Spécialise-toi dans la chirurgie esthétique.
4. Deviens célèbre dans le milieu. Plein de gens vont être intéressé·es par tes services!
5. Tire parti de leurs complexes causés par Instagram.
6. Retouche leur visage subtilement. Reste discrèt·e!
7. Certains aspects du visage sont cruciaux pour la reconnaissance faciale:
   + La forme et la position du nez (comme le nez au milieu de la figure!)
   + L'écart entre les deux yeux
   + La distance entre le front et le menton
8. Il faut que ce soit suffisament prononcé pour que les logiciels soient perturbés.
9. N'oublie pas, il faut souffrir pour être belleau.

---

# Comment parasiter la reconnaissance vocale tout en s'amusant ?

- données/métadonnées
- reconnaissance vocale
- logiciel
- intelligence artificielle

L’assistance vocale (comme Siri ou Alexa) permet d’utiliser son téléphone ou son ordinateur avec seulement sa voix, en gardant les mains libres. Sympa! Cependant, pour que ces logiciels s’activent quand on prononce leur nom, ils doivent écouter en permanence ce qui se passe autour. Comment faire pour empêcher plein de monde d'être espionné·es à leur insu?

### Méthode 1 : en organisant des soirées

Cette méthode demande un peu de préparation, mais peut-être amusante et même devenir un métier !

1. Rachète tous les stocks de pastilles pour la gorge. C'est un investissement de départ, mais augmentera l'impact de ton action à moyen terme.
2. Organise des concerts gratuits. Le but est que les gens crient un maximum, alors essaie de contacter des groupes suffisamment connus pour attirer du monde.
   + Évite les groupes du musique instrumentale, car les gens n'auront pas l'occasion de se casser la voix en chantant. Tu peux jouer sur la nostalgie ou bien opter pour des styles énergiques et vocaux, comme le *death metal* par exemple.
3. Profite de la saison ! En organisant les concerts en hiver, de préférence en extérieur, les gens vont plus facilement attraper froid, avoir un rhume ou un mal de gorge naturellement.
4. Si tout le monde s'amuse bien, beaucoup de monde aura une extinction de voix dans les jours qui viennent. Siri ne comprend plus rien mainten
5. Passe à l'internationale. Si le modèle fonctionne-bien, exporte-le ! Ça sera utile pour diffuser ton message.

## RÉACTION

Depuis le début de l'hiver 202X, un groupe d'individus organise divers concerts gratuits de heavy metal dans la ville de XXXXXXXXX. Ces concerts poussent les participant·es à fortment solliciter leur voix, entraînant très fréquemment des extinctions de voix. Nous avons remarqué une baisse des données utilisateurs concernant les échantillons de voix sur la période indiquée (voir graphique en annexe). De nombreux rapports de bugs dus à ces extinctions massives de voix ont été reçus par les équipes du service qualité ces dernières semaines. Coincidentellement, les stocks de pastilles et sprays apaisants pour traiter maux de gorges et problèmes de voix ont diminué brusquement peu avant le début des concerts. Nos sources indiquent en effet une recrudescence d'achat sur la période XXX-XXX 202X mais sans donner l'impression d'un mouvement structuré et coordonné.

En recoupant ces informations nous avons pourtant des raisons de croire qu'il s'agit d'une entreprise organisée visant à nuire, à court terme, à la réputation de l'entreprise dans le domaine de la reconnaissance vocale.

---

# Comment s'exprimer sans être espionné·e ?

- algorithme
- logiciel
- web
- NLP
- robot

L'anglais est l'une des langues les plus parlée dans le monde, et elle est aussi très présente sur le web. Cependant, puisqu'elle est très parlée elle est aussi facilement décryptée par des logiciels d'analyse de texte. Afin d'éviter cela, nous allons voir comment saboter ces algorithmes de compréhension du langage.

### Méthode 1: utilise une langue peu connue
Même si l'anglais est majoritaire, le français fait partie des 10 langues les plus utilisées sur le web. Il faut trouver quelque chose d'un peu plus original!

1. Choisis une langue construite[^]. De préférence, elle doit être peu parlée.
   + Ce que tu peux exprimer dépend beaucoup de la langue!
2. Étudie cette langue en profondeur.
3. Enseigne-là autour de toi! Il faut savoir partager.
5. Tu peux changer de langue régulièrement pour rester frai·s·che.

### Méthode 2: utilise une autre façon de parler

Les algorithmes de NLP[^] peuvent reconnaître le ton d'un texte, et donc savoir si on exprime une opinion positive ou négative d'un certain sujet.

1. Ne dis jamais ce que tu penses de manière claire.
2. Utilise des périphrases si tu dois parler d'une personne ou d'un lieu en particulier.
3. Fais des blagues! Les robots ne comprennent pas l'humour.
4. Ait l'air enjoué·e pour parler d'un sujet triste ou énervant, et inversement.


---

# Comment soutenir les micro-travailleureuses ?

- micro-travail
- plateforme
- machine learning
- réseau social

Lorsque l'on signale du contenu sur un réseau social, bien souvent ce sont des robots qui s'occupent de vérifier si c'est effectivement à censurer. Mais des fois les robots ne sont pas capables de trier correctement. Dans ce cas ce sont des micro-travailleureuses qui doivent regarder tous ces contenus choquants et dire si ça doit être retiré ou non du site. Les pauvres voient du malheur toute la journée, alors redonne-leur un peu de soleil!

### Méthode 1: en leur envoyant de la joie

1. Allume ton réseau social préféré.
2. Dirige-toi vers du contenu positif, comme des chatons mignons.
  + Internet peut aussi être plein de belles choses!
3. Signale tout ce que tu vois sur la page comme inapproprié.
  + Rajoute une description fournie, il faut que ça ait l'air d'une vraie plainte.
4. Change de page de temps en temps, les chiots aussi sont mignons.
5. Tu peux aussi signaler des articles de développement personnel ou de motivation.
6. Quelqu'un·e, quelque va sûrement trouver un peu de réconfort dans sa journée.
7. Savoure le sentiment d'avoir fait une bonne action.

---

# GLOSSAIRE

[^]: Un **SERVEUR**, c'est un ordinateur, mais sans écran et sans clavier, relié à internet. Dessus on peut stocker plein de choses, mais souvent des sites internet. En fait quand on tape l'adresse d'un site sur un navigateur, on demande à internet de connecter notre ordinateur à un serveur en particulier, et donc de montrer ce qui est stocké dessus. Mais en fait il peut y avoir plein de trucs sur un serveur, un cloud c'est aussi un serveur fialement.

[^]: Un**CLOUD** c'est un serveur de stockage de données, c'est juste un ordinateur sur lequel on peut copier des fichiers pour y avoir accès depuis internet. Quand tu met un fichier sur ton Google Drive, tu le copie sur un ordi, quelque part dans un data center, qui appartient à Google.

[^]: Bon un **DATA CENTER** c'est un gros bâtiment avec plein de serveurs (vraiment plein) qui restent allumés toute la journée. Sauf que vu qu'il ya plein d'ordis qui restent allumés tous le temps ça chauffe vite, alors il faut refroidir tout ça en utlilisant de la clim, ou alors en les construisant dans des pays où il fait plus froid (en Islande y en par exemple). Du coup ça pollue à fond parce que ça consomme plein d'énergie. Dans un data center tout appartient pas forcément au même site, mais pour les gros gros sites genre réseaux sociaux ça sert surtout à stocker toutes les données des utilisateurices. Imagine tous les messages et les photos de tous les comptes de toutes les personnes qui utilisent WhatsApp par exemple. En plus tu peux encore multiplier ça plusieurs fois parce qu'ils font des copies au cas où il y ait des serveurs qui tombent en panne ou quoi.

[^]: Les **DONNÉES** c'est plein de trucs, c'est juste des "informations". Mais ça peut être du texte, des images, des vidéos, du son, des fichiers… Ce qu'on s'échange par internet quoi, ou ce qui compose les sites. Ça peut aussi être des infos en plus ou des statistiques, et là c'est plus des métadonnées.

[^]: Les **MÉTADONNÉES** c'est des données hein, mais plus ce qu'il y a "autour" de la donnée principale. Par exemple t'envoie un mail à un·e pote et tu lui écris "salut". La donnée principale que t'envoie c'est le message, c'est "salut". Les métadonnées c'est l'adresse mail de ta·on pote, la tienne, le jour et l'heure à laquelle t'envoies le mail, l'endroit depuis où tu l'envoies, avec quel appareil/ordi tu l'envoies, des trucs du style quoi. Ça à l'air pas important mais tu peux analyser pleins de choses avec des métadonnées, plus t'en as plus tu peux faire des stats, les comparer, les croiser pour trouver ou déduire d'autres infos. Ces autres infos c'est aussi des données, qui ont aussi des métadonnées, et là ça devient le bordel, c'est la fête aux données dans tous les sens, c'est le Big Data.

[^]: Un **FICHIER** ? Euh c'est un paquet de données qu'on peut copier et envoyer à quelqu'un·e. C'est un petit tas d'informations rassemblées, qu'on peut déplacer d'un coup. Les infos qu'il y a dedans c'est souvent du code, du texte, mais qui est écrit d'une certaine façon pour que l'ordi le comprenne et sache quoi en faire.

[^]: Un **ALGORITHME** bon c'est un peu comme une recette de cuisine. T'as un problème à résoudre et pour le résoudre tu dois faire certaines actions précises dans un certain ordre, sinon ça marche pas. Si tu fais une confiture, il faut mettre les fruits dans la casserole, si t'oublie cette étape tu peux la faire chauffer tant que tu veux ta casserole il va rien se passer. Donc c'est pas forcément un truc numérique, c'est une méthode logique pour résoudre un problème. Mais les ordis c'est fait pour fonctionner avec des algorithmes, pour que si tu exécutes les étapes de l'algorithme t'arrives toujours au même résultat à la fin.

[^]: Alors un **NAVIGATEUR** c'est un logiciel qui permet d'aller sur internet. C'est genre Firefox, Chrome, Internet Explorer et tout ça. C'est pas la même chose qu'un moteur de recherche! Un site c'est du code, c'est du texte écrit d'une certaine manière. Ben le navigateur il arrive à comprendre le code, à l'interpréter pour l'afficher de la bonne manière et qu'on puisse cliquer sur les trucs, qu'il y ait les bonnes couleurs et tout ça. Parce qu'en fait on peut voir des sites sans navigateur, mais on voit juste le texte, sans mise en forme et sans pouvoir interagir avec.

[^]: Le **SYSTÈME D'EXPLOITATION** c'est un gros truc. Dans ton ordi t'as plein de parties différentes qui savent faire des choses différentes. T'as une partie qui est forte pour calculer les images, une pour capter la wifi, une pour enregistrer des informations, une pour vérifier que l'ordi surchauffe pas, une pour refroidir si ça chauffe trop… Bon tout ça marche bien séparément, mais pour que ça marche ensemble il faut un "quelque chose" qui dise aux parties comment communiquer, à quelle partie il faut envoyer de l'énergie et des informations pour effectuer l'action qu'on veut faire. Et ben le système d'exploitation c'est ça. C'est l'intermédiaire entre tes logiciels et le matériel (circuits imprimés, câbles et tout).

[^]: Un **KEYLOGGER** c'est un programme qui enregistre toutes les touches qu'on tape sur un clavier. En général c'est des virus qui font ça, parce que ça permet d'enregistrer quand tu tapes ton mot de passe par exemple donc c'est pas mal dangereux. Mais en fait certaines entreprises font ça aussi quand t'utilises leur site, pour savoir ce dont tu parles et donc te vendre des trucs qui sont censés t'intéresser.

[^]: Alors un **PROTOCOLE** c'est un genre de formule de politesse utilisée par les ordinateurs pour discuter entre eux. C'est une façon d'envoyer des infos avec une norme précise pour qu'elle soit interprétées de la bonne manière. Pour comparer, si t'écris une question la phrase commence par une majuscule et finit par un point d'interrogation, ben ça c'est un protocole. Les gens qui lisent ça comprennent que tu poses une question. Et pour avoir un exemple le mail c'est un protocole.

[^]: La **PUBLICITÉ CIBLÉE** c'est pas dingue, c'est une façon de rendre la pub plus efficace. Au lieu de montrer la même pub à tout le monde les entreprises essaient de montrer à chaque personne une pub qui pourrait l'intéresser, ellui. Si tu t'en fous des tracteurs te montrer une pub de tracteurs ça te fait ni chaud ni froid c'est sûr. Mais pour savoir ce que tu préfères il faut regarder tout le temps tout ce que tu fais et l'enregistrer.

[^]: Alors le **MOTEUR DE RECHERCHE** on pense souvent que c'est la même chose que le navigateur mais en fait non. C'est comme un gros annuaire des sites qui sont répertoriés. Quand on fait une recherche dans le moteur, ça regarde dans tout l'annuaire et ça nous donne les résultats qui correspondent "le mieux". Ça c'est en théorie mais le moteur de recherche n'est pas objectif. Pour Google ce qui correspond "le mieux" c'est d'abord les sites qui les paient pour apparaître plus haut dans la liste de résultats. Alors tous les résultats sont pas sponsorisés évidemment. On peut aller sur un site sans avoir besoin de moteur de recherche, si on connaît l'adresse exacte: l'URL (par exemple, www.lesite.com).

[^]: L'**AUTOCOMPLÉTION** c'est juste quand tu écrit sur un ordi ou un smartphone et qu'on te propose la fin du mot que tu es en train d'écrire.

[^]: Quand on parle de **ROBOT** c'est assez vague commme terme mais tu peux imaginer une machine qui exécute des actions programmées à l'avance. C'est pas seulement une machine mécanique, ça peut aussi être une machine numérique (donc un algorithme). Programme, robot et algorithme c'est plus ou moins des synonymes.

[^]: **NLP** ça veut dire Natural Language Processing, donc le traitement du langage naturel. C'est le domaine de l'informatique qui recherche comment un ordi peut "comprendre" du texte comme il pourrait être parlé par des humain·es. Parce que les ordis comprennent déjà le code informatique, qui est du texte mais écrit d'une façon très précise avec un nombre de mots limités et une grammaire pariculière. Là le NLP c'est autre chose, avec ça on peut déduire le ton d'une phrase, si la personne qui l'a écrite est en colère ou super contente.

[^]: L'**INTELLIGENCE ARTIFICIELLE** c'est quand un ordi est capable de faire des choses qui ont l'air de demander de l'intelligence, donc des choses qui ne peuvent être faites "que" par un·e humain·e. Reconnaître un visage, une voix, lire du texte dans une image, avoir une conversation, tout ça c'estdes truc qu'une machine qui a une "intelligence artificielle". Mais en fait c'est pas du tout de l'intelligence, c'est juste beaucoup de maths et de statistiques complexes. En plus un programme d'"intelligence artificielle" il sait faire qu'une seule chose très bien, il peut pas à la fois reconnaître des images et des sons. Il est hyper spécialisé.

[^]: Le **MACHINE LEARNING**, ou DEEP LEARNING c'est une forme d'intelligence artificielle. C'est une façon d'"apprendre" une compétence à un programme en l'"entraînant". Par exemple pour reconnaître des images, on donne plein (vraiment plein) d'images de chats au programme en lui précisant que sur ces images on voit un chat. Ensuite si on donne une image au programme, il peut l'analyser et dire si c'est un chat ou non. Le programme a jamais appris que c'était un chat, il a juste appris à calculer statistiquement la ressemblance d'une image qu'on lui montre avec toutes les images de chats qu'on lui a donné pendant l'entraînement. Globalement on peut utiliser ça pour "apprendre" beaucoup de choses différentes à la machine, apprendre à conduire, jouer aux échecs, reconnaître du texte, des visages, fabriquer des images… C'est bien beau tout ça mais ça rend la machine très dépendante de ce qu'elle a analysé pendant l'entraînement. Si le paquet de données était biaisé au départ, alors le machine learning ça va amplifier ce biais quand on va l'utiliser après l'entraînement. Il faut pas forcément croire que la machine a raison parce que c'est un ordinateur qui se trompe pas dans ses calculs et qu'on l'a bien entraîné.

[^]: Le **MICRO-TRAVAIL** c'est quand ton travail c'est de répéter une tâche très précise et très courte, comme par exemple cliquer sur les images qui correspondent à un mot donné. On te montre plein d'images et tu dois cliquer sur celles sur lesquelles on voit un passage piéton ou une voiture. En général ça sert à classer les données qui vont être utilisées après pour entraîner des intelligences artificielles. Une entreprise engage plein de personnes pour trier plein d'images en décrivant ce qu'on y voit, et chacune de ces personnes est payée au nombre d'images décrites. Mais micro-travail c'est aussi micro-salaire (t'es payé·e à peine 1 centime le clic), donc si tu veux gagner ta vie avec ça il faut être super rapide et travailler tout le temps.

[^]: La **RECONNAISSANCE FACIALE** c'est une technologie qui permet à un ordinateur de reconnaître un visage. Ça marche avec du MACHINE LEARNING, donc il faut entraîner un programme en lui montrant plen d'images de visages. Bon déjà il faut beaucoup de micro-travail pour que ce soit performant, et ensuite on les trouve où ces photos de visages ? On va les chercher où elles sont, alors ça peut littéralement être des centaines de milliers de selfies qui sont "récupérés" sur le net. Après la reconnaissance faciale c'est pas juste pour déverrouiller son iPhone, c'est aussi utilisé pour de la surveillance massive.

[^]: La **RECONNAISSANCE VOCALE** c'est une technologie qui permet à un ordinateur de comprendre les paroles prononcées par un·e humain·e. Ça pose les mêmes problèmes que pour tout ce qui utilise le machine learning.

[^]: Alors un **ALGORITHME DE RECOMMANDATION** c'est un algorithme utilisé pour te recommander des choses. C'est un programme qui analyse ce que tu fais sur un site par exemple (les pages que t'as visitées, combien de temps tu reste dessus, sur quoi t'a cliqué…) pour te proposer du contenu personnalisé, qui "pourrait" t'intéresser. Ça à l'air d'une bonne intention mais c'est pas juste pour te faire plaisir, c'est aussi (surtout) fait pour que tu passes plus de temps sur un site, que tu voies plus de pubs, que t'achètes plus de trucs. Accessoirement ça veut dire que t'es espionné·e aussi. La pub ciblée c'est un endroit où on utilise des algorithmes de recommandation.

---

[^]: Un **LOGICIEL** c'est

[^]: Une **PLATEFORME** c'est

[^]: **INTERNET**

[^]: **WEB**

[^]: **COOKIES**

[^]: **RÉSEAU SOCIAL**

---

